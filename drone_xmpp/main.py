#!/usr/bin/env python
"""
Drone plugin for sending XMPP notifications
"""

import datetime
import slixmpp
import os
import time
import threading


class MUCBot(slixmpp.ClientXMPP):
    def __init__(self, jid, password, room, message):
        """
        Needs it's passed parameters in order to establish a
        connection with the server. Slixmpp works executing functions when an
        event happens. In this case, we set two events, one executed when the
        connection function is passed and the other when a groupchat message is
        received.
        """
        slixmpp.ClientXMPP.__init__(self, jid, password)
        self.room = room
        self.nick = jid.split("@")[0]
        self.message = message
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("groupchat_message", self.force_exit)

    def start(self, event):
        """
        Is executed when the connect method is called. It sends
        the xmpp presence, join to a MUC and call the muc_send_message.
        """
        self.send_presence()
        self.plugin["xep_0045"].join_muc(self.room, self.nick, wait=True)
        self.muc_send_message(self.message)

    def muc_send_message(self, message):
        """
        Is executed when the bot has already joined in the MUC
        and sends the job status message.
        """
        self.send_message(mto=self.room, mbody=message, mtype="groupchat")

    def force_exit(self, message):
        """
        Is executed when a groupchat message is received. It will check if the
        message was sent by the bot and if it's the exact message it has to
        send. If both conditions are met, disconnect the bot.
        """
        if message["mucnick"] == self.nick and message["body"] == self.message:
            self.plugin["xep_0045"].leave_muc(self.room, self.nick)
            self.disconnect(wait=True)
            print("Timeout reached. Exiting...")
            # Use _exit to force the exit even with the timeout is active
            os._exit(0)


def get_build_time_str(started, finished):
    """
    Return how much time did the job take and return it.
    """
    build_start = int(started)
    build_end = int(finished)
    build_delt = build_end - build_start
    return str(datetime.timedelta(seconds=build_delt))


def get_generic_message(payload):
    """
    Return a formated message from the payload received from drone, based on
    what state is the job.
    """
    state = payload["build"]["status"]
    if state == "pending":
        message = (
            "There's an pending job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: pending\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    elif state == "running":
        message = (
            "There's a running job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: running\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    elif state in "failure":
        message = (
            "There's a failed job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: failure\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    elif state == "error":
        message = (
            "There's an error in the job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: error\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    elif state == "killed":
        message = (
            "There's a killed job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: killed\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    elif state == "success":
        message = (
            "There's a successful job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: success\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    else:
        message = (
            "There's a change in the job:\n- Repository: {repo_link}\n- "
            "Branch: {build_branch_name}\n- Link: {system_link_url}\n"
            "- State: unknown\n- Author: "
            "{build_author}\n- Message: {build_message} ({build_commit})"
        )
    return message.format(
        repo_link=payload["repo"]["link"],
        build_author=payload["build"]["author"],
        build_branch_name=payload["build"]["branch"],
        build_message=payload["build"]["message"],
        build_commit=payload["build"]["commit"],
        build_time=get_build_time_str(
            payload["build"]["started_at"], payload["build"]["finished_at"]
        ),
        system_link_url=payload["build"]["link"],
        state=state,
    )


def get_custom_message(payload):
    message = payload["message"].format(
        repo_link=payload["repo"]["link"],
        repo_pull_request=payload["repo"]["pull_request"],
        build_commit=payload["build"]["commit"],
        build_branch_name=payload["build"]["branch"],
        build_author=payload["build"]["author"],
        build_message=payload["build"]["message"],
        build_link=payload["build"]["link"],
        build_status=payload["build"]["status"],
        build_started_at=payload["build"]["started_at"],
        build_finished_at=payload["build"]["finished_at"],
        build_time=get_build_time_str(
            payload["build"]["started_at"], payload["build"]["finished_at"]
        ),
    )
    return message


def timeout(value):
    "Function to set a sane timeout."
    time.sleep(value)
    print("It's been 30 seconds and the message is not sent yet. Goodbye!")
    os._exit(1)


def get_env_vars():
    """Get environmental variables"""

    payload = {"repo": {}, "build": {}, "xmpp": {}}

    payload["repo"]["link"] = os.environ["DRONE_REPO_LINK"]
    payload["build"]["commit"] = os.environ["DRONE_COMMIT"]
    payload["build"]["message"] = os.environ["DRONE_COMMIT_MESSAGE"]
    payload["build"]["branch"] = os.environ["DRONE_BRANCH"]
    payload["build"]["author"] = os.environ["DRONE_COMMIT_AUTHOR"]
    payload["build"]["link"] = os.environ["DRONE_BUILD_LINK"]
    payload["build"]["started_at"] = os.environ["DRONE_BUILD_STARTED"]
    payload["build"]["finished_at"] = os.environ["DRONE_BUILD_FINISHED"]
    payload["build"]["status"] = os.environ["DRONE_BUILD_STATUS"]

    payload["xmpp"]["user"] = os.environ["XMPP_USER"]
    payload["xmpp"]["password"] = os.environ["XMPP_PASSWORD"]
    payload["xmpp"]["room"] = os.environ["XMPP_ROOM"]

    if "PLUGIN_MESSAGE" in os.environ:
        payload["message"] = os.environ["PLUGIN_MESSAGE"]

    if "DRONE_PULL_REQUEST" in os.environ:
        payload["repo"]["pull_request"] = os.environ["DRONE_PULL_REQUEST"]
    else:
        payload["repo"]["pull_request"] = None

    return payload


def main(payload):
    """
    Main body of the program. Will receive a drone payload, set a thread that
    will kill the program if the timeout period passes and send a formated
    message to a MUC.
    """
    # Check of there's an explicit timeout when connecting to xmpp server
    # And set it to 30 if it doesn't
    if "timeout" in payload["xmpp"]:
        xmpp_timeout = payload["xmpp"]["timeout"]
    else:
        xmpp_timeout = 30
    # Check if XMPP variables are defined
    if "user" not in payload["xmpp"]:
        print("You have to define a XMPP user")
        return 1
    if "password" not in payload["xmpp"]:
        print("You have to define a XMPP password")
        return 1
    if "room" not in payload["xmpp"]:
        print("You have to define a XMPP room")
        return 1
    action_thread = threading.Thread(target=timeout, args=[xmpp_timeout])
    action_thread.start()
    if "message" in payload:
        message = get_custom_message(payload)
    else:
        message = get_generic_message(payload)
    xmpp = MUCBot(
        payload["xmpp"]["user"],
        payload["xmpp"]["password"],
        payload["xmpp"]["room"],
        message,
    )
    xmpp.register_plugin("xep_0045")
    xmpp.connect()
    xmpp.process()


if __name__ == "__main__":  # pragma: no cover
    payload = get_env_vars()
    main(payload)
