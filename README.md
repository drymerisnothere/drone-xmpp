drone-xmpp
==========

Drone plugin for sending XMPP notifications.

[![Build Status](https://drone.daemons.it/api/badges/drymer/drone-xmpp/status.svg)](https://drone.daemons.it/drymer/drone-xmpp)

Requirements
------------

Slixmpp == 1.3.0

Python 3.4, 3.5, 3.6

Overview
--------

Run the plugin directly after installing requirements:

```bash
DRONE_REPO='drymer/drone-xmpp' DRONE_COMMIT='9f2849d5' \
DRONE_COMMIT_MESSAGE='The drone-xmpp plugin works correctly' DRONE_BRANCH='master' \
DRONE_COMMIT_AUTHOR='drymer' DRONE_BUILD_LINK='http://localhost:3000' \
DRONE_BUILD_STARTED='1421029603' DRONE_BUILD_FINISHED='1421029813' \
DRONE_BUILD_STATUS='success' XMPP_USER='test@test.org' \
XMPP_PASSWORD='test' XMPP_ROOM='ic@test.org' \
python3 drone_xmpp/main.py
```

Docker
------

Alternatively, run the plugin directly from a built docker image:

```bash
docker run -i --rm --name drone-xmpp -e DRONE_REPO='drymer/drone-xmpp' -e \
    DRONE_COMMIT='9f2849d5' -e \
    DRONE_COMMIT_MESSAGE='The drone-xmpp plugin works correctly' -e \
    DRONE_BRANCH='master' -e DRONE_COMMIT_AUTHOR='drymer' -e \
    DRONE_BUILD_LINK='http://localhost:3000' -e \
    DRONE_BUILD_STARTED='1421029603' -e DRONE_BUILD_FINISHED='1421029813' -e \
    DRONE_BUILD_STATUS='success' -e XMPP_USER='test@test.org' -e \
    XMPP_PASSWORD='test' -e XMPP_ROOM='test@test.org' \
    registry.daemons.it/drone-xmpp
```

Messages
--------

By default, the plugin sends a message like this:

```text
There's a successful job:
- Repository: http://localhost/drymer/drone-xmpp
- Branch: master
- Link: http://localhost:3000
- State: success
- Author: drymer
- Message: The drone-xmpp plugin works correctly (9f2849d5)
```

But you can use a custom message. It supports the next variables:
- **repo_link**: The git repository link.
- **build_commit**: The git commit hash that activates the hook.
- **build_branch_name**: The git branch that activates the hook.
- **build_author**: The git push author.
- **build_message**: The git commit message that activates the hook.
- **build_link**: The drone build link.
- **build_status**: The drone build status.
- **build_started_at**: The started time of the drone build in epoch format.
- **build_finished_at**: The the ended time of the drone build in epoch format.
- **build_time**: The total build time.

You may use it like this:

```bash
docker run -i --rm --name drone-xmpp -e DRONE_REPO='drymer/drone-xmpp' -e \
    DRONE_COMMIT='9f2849d5' -e \
    DRONE_COMMIT_MESSAGE='The drone-xmpp plugin works correctly' -e \
    DRONE_BRANCH='master' -e DRONE_COMMIT_AUTHOR='drymer' -e \
    DRONE_BUILD_LINK='http://localhost:3000' -e \
    DRONE_BUILD_STARTED='1421029603' -e DRONE_BUILD_FINISHED='1421029813' \
    -e DRONE_BUILD_STATUS='success' -e XMPP_USER='test@test.org' -e \
    XMPP_PASSWORD='test' -e XMPP_ROOM='test@test.org' -e \
    MESSAGE='This build took {build_time} seconds.'\
    registry.daemons.it/drone-xmpp
```

Tests
-----

There's two kind of tests, unitary and integration tests.

### Unitary tests

To run the unitary tests, you need docker or the python versions 3.4,
3.5 and 3.6 plus tox. To execute them with docker:

```bash
make test-unit
```

### Integration tests

This test just builds the docker image and use it. If it finishes
correctly, is assumed that all went correctly:

```bash
make test-int
```

Other
-----

Execute the next order to see what you can do:

```bash
make help
```

License
-------

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see &lt;<http://www.gnu.org/licenses/>&gt;.
