.PHONY: help

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  clean                       remove *.pyc files, __pycache__ and *.egg-info directories"
	@echo "  clean-docker                remove docker containers, volumes and image"
	@echo "  test-unit                   execute unitary tests with tox"
	@echo "  test-int                    execute integration tests with ansible"
	@echo "  build                       build the docker image"
	@echo "  push                        push the docker image"
	@echo "Check the Makefile to know exactly what each target is doing."

clean:
	@echo "Deleting '*.pyc', '__pycache__' and '*.egg-info'..."
	@find . -name '*.pyc' -delete
	@find . -name '__pycache__' -type d | xargs rm -fr
	@find . -name '*.egg-info' -type d | xargs rm -fr

clean-docker:
	@echo "Cleaning remaining docker stuff..."
	@# Delete image
	@if [ -n "$$(docker images --filter reference=r.daemons.it/drone-xmpp | grep -v REPOSITORY)" ]; then \
		docker rmi -f r.daemons.it/drone-xmpp; \
	fi
	@if [ -n "$$(docker images --filter reference=test/drone-xmpp | grep -v REPOSITORY)" ]; then \
		docker rmi -f test/drone-xmpp; \
	fi
	@# Delete containers
	@if [ -n "$$(docker ps -aq --filter name=data-container-tox)" ]; then \
		docker rm -f data-container-tox; \
	fi
	@if [ -n "$$(docker ps -aq --filter name=data-container-ansible)" ]; then \
		docker rm -f data-container-ansible; \
	fi
	@if [ -n "$$(docker ps -q --filter name=drone-xmpp-tox)" ]; then \
		docker rm -f $(docker ps -q --filter "name=drone-xmpp-tox"); \
	fi
	@if [ -n "$$(docker ps -q --filter name=drone-xmpp-ansible)" ]; then \
		docker rm -f $(docker ps -q --filter "name=drone-xmpp-ansible"); \
	fi
	@# Delete volumes
	@if [ -n "$$(docker volume ls -f name=drone-xmpp-tox| grep -v DRIVER)" ]; then \
		docker volume rm -f drone-xmpp-tox; \
	fi
	@if [ -n "$$(docker volume ls -f name=drone-xmpp-ansible | grep -v DRIVER)" ]; then \
		docker volume rm -f drone-xmpp-ansible; \
	fi

test-unit:
	docker volume create --name drone-xmpp-tox
	docker run --name data-container-tox -v drone-xmpp-tox:/root/files alpine
	@# Copy the files on path to a volume in a very non-efficient way because fuck you
	for file in $$(ls -A --color=none); do \
		docker cp $$file data-container-tox:/root/files;  \
	done
	exit
	docker cp .coveragerc data-container-tox:/root/files
	docker pull registry.daemons.it/tox:root
	docker run --name drone-xmpp-tox --tty -v drone-xmpp-tox:/root/files --rm \
		registry.daemons.it/tox:root tox

test-int:
	/bin/sh -i tests/int/test.sh

build:
	docker build -t r.daemons.it/drone-xmpp:latest --target production .

push:
	docker push r.daemons.it/drone-xmpp:latest

all: clean test-unit test-int build
