FROM alpine:3.7 as builder
RUN mkdir -p /var/cache/apk && ln -s /var/cache/apk /etc/apk/cache && apk add \
    --update python3 python3-dev gcc musl-dev
COPY requirements.txt /root/
RUN pip3 install wheel && pip3 wheel --wheel-dir=/root/wheel -r \
    /root/requirements.txt

FROM alpine:3.7 as production
COPY --from=builder /root/wheel /root/wheel
COPY --from=builder /root/.cache /root/.cache
COPY --from=builder /etc/apk/cache /etc/apk/cache
RUN apk add python3 python3-dev
COPY requirements.txt /root/
RUN pip3 install --no-index --find-links=/root/wheel -r /root/requirements.txt
RUN rm -rf /root/.cache /root/requirements.txt /etc/apk/cache/* /root/wheel/
COPY drone_xmpp /opt/drone/plugin/
ENTRYPOINT ["python3", "/opt/drone/plugin/main.py"]

FROM alpine:3.7 as debug
COPY --from=builder /root/wheel /root/wheel
COPY --from=builder /root/.cache /root/.cache
COPY --from=builder /etc/apk/cache /etc/apk/cache
COPY --from=production /opt/drone/ /opt/drone/
COPY requirements.txt /root/
RUN apk add python3 python3-dev musl-dev gcc
RUN pip3 install wheel && pip3 wheel --wheel-dir=/root/wheel ipdb -r \
    /root/requirements.txt && pip3 install --no-index --find-links=/root/wheel \
    ipdb -r /root/requirements.txt
ENTRYPOINT ["python3", "/opt/drone/plugin/main.py"]
