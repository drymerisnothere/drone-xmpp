#!/usr/bin/env python3

from setuptools import setup

VERSION = '0.1'

setup(name='drone-xmpp',
      version=VERSION,
      description='Post drone events to xmpp.',
      author='drymer',
      author_email='drymer@autistici.org',
      url='https://git.daemons.it/drymer/drone-xmpp/',
      license="GPLv3",
      install_requires=[
          "slixmpp==1.3.0",
          "drone==0.3.0",
          ],
      classifiers=["Development Status :: 4 - Beta",
                   "Programming Language :: Python :: 3",
                   "Operating System :: OS Independent",
                   "Operating System :: POSIX",
                   "Intended Audience :: End Users/Desktop"]
      )
