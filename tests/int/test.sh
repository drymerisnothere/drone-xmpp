#!/bin/sh -i

set -ex

docker build -t test/drone-xmpp --target production .
docker run -i --rm --name drone-xmpp -e \
    DRONE_REPO_LINK='http://localhost/drymer/drone-xmpp' -e \
    DRONE_COMMIT='9f2849d5' -e \
    DRONE_COMMIT_MESSAGE='The drone-xmpp plugin works correctly' -e \
    DRONE_BRANCH='master' -e DRONE_COMMIT_AUTHOR='drymer' -e \
    DRONE_BUILD_LINK='http://localhost:3000' -e \
    DRONE_BUILD_STARTED='1421029603' -e DRONE_BUILD_FINISHED='1421029813' -e \
    DRONE_BUILD_STATUS='success' -e XMPP_USER='testeando@chat.cslabrecha.org' -e \
    XMPP_PASSWORD='testeando' -e \
    XMPP_ROOM='ic@sala.chat.cslabrecha.org' test/drone-xmpp
docker run -i --rm --name drone-xmpp -e \
    DRONE_REPO_LINK='http://localhost/drymer/drone-xmpp' -e \
    DRONE_COMMIT='9f2849d5' -e \
    DRONE_COMMIT_MESSAGE='The drone-xmpp plugin works correctly' -e \
    DRONE_BRANCH='master' -e DRONE_COMMIT_AUTHOR='drymer' -e \
    DRONE_BUILD_LINK='http://localhost:3000' -e \
    DRONE_BUILD_STARTED='1421029603' -e DRONE_BUILD_FINISHED='1421029813' -e \
    DRONE_BUILD_STATUS='success' -e XMPP_USER='testeando@chat.cslabrecha.org' -e \
    XMPP_PASSWORD='testeando' -e \
    XMPP_ROOM='ic@sala.chat.cslabrecha.org' -e \
    PLUGIN_MESSAGE='This is a custom message. The DRONE_COMMIT is {build_commit}' \
    test/drone-xmpp
set +ex
