from drone_xmpp.main import get_build_time_str
from drone_xmpp.main import get_generic_message
from drone_xmpp.main import get_custom_message
from drone_xmpp.main import timeout
from drone_xmpp.main import MUCBot
from drone_xmpp.main import main
from drone_xmpp.main import get_env_vars
import pytest
import copy
import unittest.mock


PAYLOAD = {'repo': {}, 'build': {}, 'xmpp': {}}
PAYLOAD['repo']['link'] = 'https://localhost/drymer/drone-xmpp'
PAYLOAD['repo']['pull_request'] = None
PAYLOAD['build']['commit'] = 'cd596e9b1a'
PAYLOAD['build']['branch'] = 'master'
PAYLOAD['build']['author'] = 'drymer'
PAYLOAD['build']['message'] = 'The drone-xmpp plugin works correctly'
PAYLOAD['build']['link'] = 'http://localhost'
PAYLOAD['build']['status'] = '{status}'
PAYLOAD['build']['started_at'] = '1421029603'
PAYLOAD['build']['finished_at'] = '1421029813'
PAYLOAD['xmpp']['user'] = 'testeando@chat.cslabrecha.org'
PAYLOAD['xmpp']['password'] = '12345'
PAYLOAD['xmpp']['room'] = 'brechadigital@sala.chat.cslabrecha.org'

ENVIRON_WITHOUT_MESSAGE = {
    'DRONE_REPO_LINK': 'https://localhost/drymer/drone-xmpp',
    'DRONE_COMMIT': '9f2849d5',
    'DRONE_COMMIT_MESSAGE': 'The drone-xmpp plugin works correctly',
    'DRONE_BRANCH': 'master',
    'DRONE_COMMIT_AUTHOR': 'drymer',
    'DRONE_BUILD_LINK': 'http://localhost:3000',
    'DRONE_BUILD_STARTED': '1421029603',
    'DRONE_BUILD_FINISHED': '1421029813',
    'DRONE_BUILD_STATUS': 'success',
    'XMPP_USER': 'test@test.com',
    'XMPP_PASSWORD': 'test',
    'XMPP_ROOM': 'test@muc.test.com'
}

ENVIRON_WITH_MESSAGE = {
    'DRONE_REPO_LINK': 'https://localhost/drymer/drone-xmpp',
    'DRONE_COMMIT': '9f2849d5',
    'DRONE_COMMIT_MESSAGE': 'The drone-xmpp plugin works correctly',
    'DRONE_BRANCH': 'master',
    'DRONE_COMMIT_AUTHOR': 'drymer',
    'DRONE_BUILD_LINK': 'http://localhost:3000',
    'DRONE_BUILD_STARTED': '1421029603',
    'DRONE_BUILD_FINISHED': '1421029813',
    'DRONE_BUILD_STATUS': 'success',
    'XMPP_USER': 'test@test.com',
    'XMPP_PASSWORD': 'test',
    'XMPP_ROOM': 'test@muc.test.com',
    'PLUGIN_MESSAGE': 'This is a custom message. I want to substitute the' +
    ' {repo_link} and the {build_message}'
}

STATUS = ["pending", "running", "failure", "killed", "success", "error", "nop"]

BOT_DATA = [
    "test@false.com", "fakePassword", "bdue@false.com", "fake message"
]

MESSAGE = "There's a successful job:\n- Repository: https://localhost/drymer/d"
MESSAGE += "rone-xmpp\n- Branch: master\n- Link: http://localhost\n- State: su"
MESSAGE += "ccess\n- Author: drymer\n- Message: The drone-xmpp plugin works co"
MESSAGE += "rrectly (cd596e9b1a)"


# Test MUCBot class
class TestMUCBot(unittest.TestCase):
    @unittest.mock.patch('slixmpp.xmlstream.xmlstream.XMLStream.'
                         'add_event_handler')
    @unittest.mock.patch('slixmpp.ClientXMPP.__init__')
    def setUp(self, clientxmpp, add_event_handler):
        self.jid, self.password, self.room, self.message = BOT_DATA
        self.nick = self.jid.split('@')[0]
        self.testbot = MUCBot(
            self.jid, self.password, self.room, self.message
        )
        self.clientxmpp = clientxmpp
        self.add_event_handler = add_event_handler
        self.mock_join_muc = unittest.mock.MagicMock()
        self.testbot.plugin = {'xep_0045': self.mock_join_muc}
        self.testbot.default_ns = None
        self.testbot.use_message_ids = None
        self.testbot.default_lang = None
        self.testbot._XMLStream__filters = None
        self.os_exit_patch = unittest.mock.patch(
            'os._exit',
            autospect=True
        )
        self.os_exit = self.os_exit_patch.start()
        self.disconnect_patch = unittest.mock.patch(
            'slixmpp.xmlstream.xmlstream.XMLStream.disconnect',
            autospect=True
        )
        self.disconnect = self.disconnect_patch.start()

    def tearDown(self):
        self.os_exit.stop()
        self.disconnect.stop()

    def test_init(self):
        self.clientxmpp.assert_called_once_with(
            self.testbot,
            self.jid,
            self.password
        )

        assert self.add_event_handler.call_count == 2

        calls = [
            unittest.mock.call("groupchat_message", self.testbot.force_exit),
            unittest.mock.call("session_start", self.testbot.start)
        ]
        self.add_event_handler.assert_has_calls(calls, any_order=True)

        assert self.room == 'bdue@false.com'
        assert self.nick == 'test'
        assert self.message == 'fake message'

    @unittest.mock.patch('slixmpp.BaseXMPP.send_presence')
    @unittest.mock.patch('drone_xmpp.main.MUCBot.muc_send_message')
    def test_start(self, muc_send_message, send_presence):
        self.testbot.start(None)
        assert send_presence.call_count == 1
        muc_send_message.assert_called_once_with(self.message)
        self.testbot.plugin['xep_0045'].join_muc.assert_called_once_with(
            self.room,
            self.nick,
            wait=True
        )

    # Test that the mocked function is is called with it's arguments
    @unittest.mock.patch('slixmpp.BaseXMPP.send_message')
    def test_muc_send_message(self, send_message):
        self.testbot.muc_send_message(self.message)
        send_message.assert_called_once_with(
            mto=self.room,
            mbody=self.message,
            mtype='groupchat'
        )

    def test_correct_force_exit(self):
        muc_message = {'mucnick': self.nick, 'body': self.message}
        self.testbot.force_exit(muc_message)
        self.testbot.plugin['xep_0045'].leave_muc.assert_called_once_with(
            self.room,
            self.nick
        )
        self.disconnect.assert_called_once_with(wait=True)
        self.os_exit.assert_called_once_with(0)

    def test_incorrect_force_exit(self):
        muc_message = {'mucnick': 'false', 'body': self.message}
        self.testbot.force_exit(muc_message)
        assert self.testbot.plugin['xep_0045'].leave_muc.call_count == 0
        assert self.disconnect.call_count == 0
        assert self.os_exit.call_count == 0


class TestMain(unittest.TestCase):
    def setUp(self):
        self.mucbot_patch = unittest.mock.patch(
            'drone_xmpp.main.MUCBot',
            autospect=True
        )
        self.mucbot = self.mucbot_patch.start()
        self.thread_patch = unittest.mock.patch(
            'threading.Thread',
            autospect=True
        )
        self.thread = self.thread_patch.start()

    def test_main_works_correctly(self):
        payload = copy.deepcopy(PAYLOAD)
        main(payload)
        assert self.thread.call_count == 1
        assert self.mucbot.call_count == 1

    def test_main_fails_without_user(self):
        payload = copy.deepcopy(PAYLOAD)
        del payload['xmpp']['user']
        payload['build']['status'] = payload['build']['status'].format(
            status="success"
        )
        assert main(payload) == 1
        assert self.thread.call_count == 0
        assert self.mucbot.call_count == 0

    def test_main_fails_without_password(self):
        payload = copy.deepcopy(PAYLOAD)
        del payload['xmpp']['password']
        payload['build']['status'] = payload['build']['status'].format(
            status="success"
        )
        assert main(payload) == 1
        assert self.thread.call_count == 0
        assert self.mucbot.call_count == 0

    def test_main_fails_without_room(self):
        payload = copy.deepcopy(PAYLOAD)
        del payload['xmpp']['room']
        payload['build']['status'] = payload['build']['status'].format(
            status="success"
        )
        assert main(payload) == 1
        assert self.thread.call_count == 0
        assert self.mucbot.call_count == 0

    def test_main_xmpp_with_explicit_timeout(self):
        payload = copy.deepcopy(PAYLOAD)
        payload['build']['status'] = payload['build']['status'].format(
            status="success"
        )
        xmpp = payload['xmpp']
        xmpp['timeout'] = 10
        main(payload)
        self.thread.assert_called_once_with(
            target=timeout,
            args=[10]
        )
        self.mucbot.assert_called_once_with(
            xmpp["user"],
            xmpp["password"],
            xmpp["room"],
            MESSAGE
        )

    def test_main_xmpp_without_explicit_timeout(self):
        payload = copy.deepcopy(PAYLOAD)
        payload['build']['status'] = payload['build']['status'].format(
            status="success"
        )
        xmpp = payload['xmpp']
        main(payload)
        # 30 is the default value when timeout is not explicitly declared
        self.thread.assert_called_once_with(
            target=timeout,
            args=[30]
        )
        self.mucbot.assert_called_once_with(
            xmpp["user"],
            xmpp["password"],
            xmpp["room"],
            MESSAGE
        )

    def test_main_custom_message(self):
        payload = copy.deepcopy(PAYLOAD)
        payload['message'] = "This is a custom message. The DRONE_COMMIT is' + \
        '{build_commit}"
        main(payload)
        assert self.thread.call_count == 1
        assert self.mucbot.call_count == 1


def test_get_build_time_str():
    time = get_build_time_str(
        PAYLOAD['build']['started_at'],
        PAYLOAD['build']['finished_at'],
    )
    assert time == '0:03:30'


@pytest.mark.parametrize("state", STATUS)
def test_get_generic_message(state):
    payload = copy.deepcopy(PAYLOAD)
    payload['build']['status'] = payload['build']['status'].format(
        status=state
    )
    message = get_generic_message(payload)
    if state == "pending":
        message_part = "There's an pending job"
        assert message_part in message
    elif state == "running":
        message_part = "There's a running job"
        assert message_part in message
    elif state == "failure":
        message_part = "There's a failed job"
        assert message_part in message
    elif state == "error":
        message_part = "There's an error in the job"
        assert message_part in message
    elif state == "killed":
        message_part = "There's a killed job"
        assert message_part in message
    elif state == "success":
        message_part = "There's a successful job"
        assert message_part in message
    else:
        message_part = "There's a change in the job"
        assert message_part in message


def test_get_custom_message():
    payload = copy.deepcopy(PAYLOAD)
    payload['message'] = 'This is a custom message. I want to substitute t' + \
        'he {repo_link} and the {build_message}'

    message = get_custom_message(payload)
    custom_message = 'This is a custom message. I want to substitute the ' + \
        'https://localhost/drymer/drone-xmpp and the The drone-xmpp plugin' + \
        ' works correctly'
    assert message == custom_message


@unittest.mock.patch.dict('os.environ', ENVIRON_WITHOUT_MESSAGE)
def test_get_env_vars_without_custom_message():
    payload = get_env_vars()
    assert payload['repo']['link'] == 'https://localhost/drymer/drone-xmpp'
    assert payload['repo']['pull_request'] is None
    assert payload['build']['commit'] == '9f2849d5'
    assert payload['build']['message'] == 'The drone-xmpp plugin works' \
        + ' correctly'
    assert payload['build']['branch'] == 'master'
    assert payload['build']['author'] == 'drymer'
    assert payload['build']['link'] == 'http://localhost:3000'
    assert payload['build']['started_at'] == '1421029603'
    assert payload['build']['finished_at'] == '1421029813'
    assert payload['build']['status'] == 'success'
    assert payload['xmpp']['user'] == 'test@test.com'
    assert payload['xmpp']['password'] == 'test'
    assert payload['xmpp']['room'] == 'test@muc.test.com'


@unittest.mock.patch.dict('os.environ', ENVIRON_WITH_MESSAGE)
def test_get_env_vars_with_custom_message():
    payload = get_env_vars()
    assert payload['repo']['link'] == 'https://localhost/drymer/drone-xmpp'
    assert payload['repo']['pull_request'] is None
    assert payload['build']['commit'] == '9f2849d5'
    assert payload['build']['message'] == 'The drone-xmpp plugin works' \
        + ' correctly'
    assert payload['build']['branch'] == 'master'
    assert payload['build']['author'] == 'drymer'
    assert payload['build']['link'] == 'http://localhost:3000'
    assert payload['build']['started_at'] == '1421029603'
    assert payload['build']['finished_at'] == '1421029813'
    assert payload['build']['status'] == 'success'
    assert payload['xmpp']['user'] == 'test@test.com'
    assert payload['xmpp']['password'] == 'test'
    assert payload['xmpp']['room'] == 'test@muc.test.com'
    assert payload['message'] == 'This is a custom message. I want to substit'\
        + 'ute the {repo_link} and the {build_message}'


@unittest.mock.patch('time.sleep')
@unittest.mock.patch('os._exit')
def test_timeout(os_exit, time_sleep):
    timeout(30)
    time_sleep.assert_called_once_with(30)
    os_exit.assert_called_once_with(1)
